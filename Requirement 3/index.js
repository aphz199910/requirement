/**
 * _Input: Number array [1- 100]			
_Output: find pairs of numbers whose sum is prime and arrange them from small to large.	
Note: Không được trùng vd: [1,2] thì không được có [2,1]			
 */

function range(start, end) {
    return Array(end - start + 1).fill().map((_, idx) => start + idx)
}
let result = range(1, 100);

let arr1 = []; // Mảng lớn chứa các phần tử là các mảng nhỏ

for (let i = 0; i < 100; i++) {
    for (let l = 0; l < 100; l++) {
        let pt1 = result[i], pt2 = result[l];   //pt1: phan tu 1 / pt2: phan tu 2
        if (result[i] > result[l]){
            continue
        } 
        let tong = pt1 + pt2;

        // Biến cờ hiệu
        let flag = true;
 
        if (tong < 2 || tong % 2 == 0){
            flag = false;   // Nếu n bé hơn 2 tức là không phải số nguyên tố
        }
        else if (tong == 2){
            flag = true;
        }
         else{
            // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
            for (let i = 3; i < tong-1; i+=2)
            {
                if (tong % i == 0){
                    flag = false;
                    break;
                }
            }
        }
        
        if (flag == true) {
            let arr = [pt1,pt2];
            arr1.push(arr) 

        }
        
    }
}
console.log(arr1)

const arr = arr1.sort((a,b)=>{
    return (a.reduce((total,curent)=>{return total+=curent})) - (b.reduce((total,curent)=>{return total+=curent}));
})
console.log(arr);