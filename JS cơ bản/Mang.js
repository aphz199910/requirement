/**
 Mảng trong JavaScript - Array

 1. Tạo mảng
    - Cách tạo
    - Kiểm tra data type

2. Truy suất mảng
    - Độ dài mảng
    - Lấy phần tử theo index
 */


    let languages = [
        'JavaScript',
        'Ruby',
        'PHP'
    ];
// Array có thể chứa tất cả các kiểu dữ liệu trong JS
    console.log(languages)


// Cách kiểm tra đối tượng có phải là 1 array hay không
console.log(Array.isArray(languages));

// Đếm độ dài mảng
console.log(languages.length); // Mảng có 3 phần tử 0,1,2

//Lấy phần tử theo index    (index vị trí của phần tử trong mảng)
console.log(languages[index]);  //index: 0,1,2,.....


/**
 Làm việc với mảng
 1. To String / join
 2. Pop
 3. Push
 4. Shift
 5. Unshift
 6. Splicing
 7. Concat
 8. Slicing
    Học kĩ hơn về Array: Javasscript array methods
 */

    // 1. toString / join: chuyển kiểu dữ liệu từ Array sang String
    console.log(languages.toString());  
    // 'javaScript,Ruby,PHP'
    //  Muốn thay đổi dấu phẩy thành 1 dấu khác thì sử dụng join
    console.log(languages.join());
    /**
     Nếu không truyền dấu nào vào join() thì sẽ không khác gì sử dụng toString
        console.log(languages.join('-'));
        'javaScript-Ruby-PHP'
        console.log(languages.join('_'));
        'javaScript_Ruby_PHP'
     */


    // 2. pop: Xóa đi phần tử ở cuối mảng và trả về phần tử đó 
    console.log(languages.pop());   // PHP
    console.log(languages);

    // 3. Push: thêm 1 hoặc nhiều phần tử ở cuối mảng và trả về độ dài mới của mảng 
    console.log(languages.push('Dart', 'Java'));    //Thêm vào cuối mảng ngôn ngữ Dart, Java
    console.log(languages);

    // 4. Shift: xóa đi 1 phần tử ở đầu mảng và trả về phần tử đã xóa
    console.log(languages.shift()); // JavaScript
    console.log(languages);

    // 5. Unshift: thêm 1 hoặc nhiều phần tử vào đầu mảng và trả về độ dài mới của mảng
    console.log(languages.unshift('Dart', 'Java')); //Thêm vào đầu mảng ngôn ngữ Dart, Java
    console.log(languages);

    // 6. Splicing: Thêm / xóa 1 hoặc nhiều phần tử vào bất kì vị trí nào trong mảng
    languages.splice(1, 0, 'Dart', 'Java');
    console.log(languages);
    /**
     * Tham số 1: vị trí muốn thêm hoặc xóa phần tử
     * Tham số 2: số phần tử muốn xóa tính từ vị trí dặt con trỏ (đặt là 0 nếu không muốn xóa)
     * Tham số 3: những phần tử muốn thêm vào mảng tính từ vị trí đặt con trỏ
     */

    // 7. Concat: nối mảng thứ 2 vào mảng 1 trong trường hợp có 2 mảng 
    let languages2 = [
       'Dart',
       'Java'
    ];
    console.log(languages.concat(languages2));
    // Nối mảng languages2 vào languages đổi vị trí 2 mảng nếu muốn nối ngược lại

    // 8. Slicing: cắt 1, nhiều hoặc tất cả phần tử trong mảng và trả về các phần tử mới đc cắt 
    console.log(languages.slice(start, end));  
    // Start: vị trí phần tử bắt đầu cắt
    // End: vị trí phần tử ngừng cắt
    // Nếu truyền vào slice(0); thì mảng sẽ đc cắt hết tương tự như coppy mảng 