/**
 * Khai báo hàm bắt đầu bằng 'function' + Tên hàm
 * Quy tắc dặt tên hàm
 * a-z
 * A-Z
 * 0-9
 * '_'
 * '$'
 */
// Arguments: chỉ đc gọi trong 1 function
// function writeLog () {
//     console.log(arguments)
// }
// writeLog('Log 1', 'Log 2', 'Log 3')

// Giới thiệu vòng for of
/**
 function writeLog (){
    for (let param of arguments){
        myString += `${param} - `
        console.log(myString)
    }
}
writeLog('Log 1', 'Log 2', 'Log 3')
 */

// Return trong hàm
let isConfirm = confirm('message');
console.log(isConfirm)