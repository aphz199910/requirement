//var: khai báo biến
var fullName = 'pham phuong an'
var age = '21'

//alert: in thông báo topup
alert(fullName);
alert(age);

//in trong tab console
console.log (age)

//in top up xác nhận có/không
confirm('xac nhan ban da du tuoi')

//in topup thon bao cho phep dien ket qua
prompt ('xac nhan ban du tuoi')

//thuc thi lenh trong {} sau 1 khoang thoi gian dem lui tinh bang mili giay
//setTimeout (function() {alert('thong bao muon in')}, khoang thoi gian)
setTimeout (function() {alert('thong bao')}, 3000)

//tuong tu setTimeout nhung se thuc thi lenh trong {} lap di lap lai sau 1 khong thoi gian
setInterval (function () {console.log (fullName)}, 3000)

/*
 +  Cộng
 -  Trừ
 *  Nhân
 /  Chia
 ** Lũy thừa
 %  Chia lấy số dư
 ++ Tăng 1 giá trị số
 -- Giảm 1 giá trị số
 */


 /*
  * tiền tố và hậu tố. đối với tiền tố các toán tử vẫn có ý nghĩa như bình thường
  Ví dụ đối với hậu tố
  */  
  var a = 6;
// việc 1: 'a copy', 'a copy' = 6
// việc 2: trừ 1 của a, a = a - 1 => a = 5
// việc 3: trả về a copy
  var output = a++;
  console.log ('output: ', output); //output: a = 6
  console.log ('a ', a);    //output: a = 7


  /*
  Toán tử so sánh

  ==        Bằng
  !=        Không bằng
  >         Lớn hơn
  <         Nhỏ hơn
  >=        Lớn hơn hoặc bằng
  <=        Nhỏ hơn hoặc bằng

  */


  // Kiểu dữ liệu Boolean
   var a = 1;
   var b = 2;
   var isSuccess = a > b
   console.log (isSuccess); //false
   // Kiểu dữ liệu boolean chỉ trả về 2 giá trị true / false
/**
 var isSuccess = true;
 console.log (isSuccess);   //true
 */

 //Template String ES
 var firstName = 'An';
 var lastName = 'Pham';
 console.log(`Toi la: ${firstName} ${lastName}`);