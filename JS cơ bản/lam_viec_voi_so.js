// Cách kiểm tra phép toán của người dùng nhập vào có hợp lệ hay không

let result = 20 / 'ABC';
console.log(isNaN(result));     //true / false

// làm việc với số

let age = 18;
let PI = 3.14;

// CHuyển đổi kiểu number thành kiểu String
console.log(age.toString());    //Kết quả: '18' 

// Làm tròn số thập phân
console.log(PI.toFixed()); // làm tròn xuống nếu dưới 3.5 làm tròn lên nếu trên 3.5

