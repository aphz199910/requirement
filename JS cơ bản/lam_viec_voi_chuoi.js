//Làm việc với chuỗi
var myString = 'Pham Phuong An';

// 1. Length (Đo độ dài chuỗi)
console.log(myString.length)


// 2. Find index (Tìm vị trí của 1 kí tự trong chuỗi)
console.log(myString.indexOf('n'));
console.log(myString.lastIndexOf('n'));
// Tương tự indexOf nhưng hỗ trợ tìm kiếm biểu thức chính quy
//console.log(myString.search(''));
 

// 3. Cut string (cắt 1 kí tự hoặc 1 đoạn trong chuôi)
//console.log(myString.slice(vị trí 1, vị trí 2));
// cắt chuỗi theo chiều ngược lại bằng cách đổi các vị trí thành giá trị âm và đếm ngược lại


// 4. Replace (ghi đè)
console.log(myString.replace('An', 'Pham')); 
/*
 * thay chữ 'An' trong chuỗi bằng chữ 'Pham'
 * Nếu trong chuỗi có nhiều kí tự giống nhau và muốn thay đổi
 tất cả các ký tự ấy 1 lúc thì làm như sau:
 console.log(myString.replace(/a/g, 'o'));
 Sử dụng biểu thức chính quy  /ký tự muốn thay đổi/g
 */

// 5. Convert to upper case & Convert to lower case
// Chuyển chữ thường thành chữ hoa
console.log(myString.toUpperCase());

// Chuyển chữ hoa thành chữ thường
console.log(myString.toLowerCase());

// 6. Trim (Loại bỏ các kí tự trắng thừa trong chuỗi người dùng nhập)
console.log(myString.trim());

//Split (quy đổi danh sách từ dạng String sang dạng Array)
// Phải có điểm chung giữa các phần từ trong chuỗi 
// Điểm chung của chuỗi ví dụ dưới đây là dấu ", "
var languages = 'Java Script, PHP, Ruby';
console.log(languages.split(', ' )); 
// Nếu truyền vào split() là 1 chuỗi rỗng thì nó sẽ cắt mỗi kĩ tự trong chuỗi thành 1 phần tử của mảng


// 8. Gat a character by index (lấy đc 1 kí tự trong chuỗi bởi vị trí cho trước)
const myString2 = 'Pham Phuong An';
console.log(myString2.charAt(0));
// truyền vào charAt vị trí kí tự muốn lấy 0,1,2,3,...





 