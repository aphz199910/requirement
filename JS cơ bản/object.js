// object trong JS

let myInfo = {
    name: 'phuong an',
    age: 21,
    address: 'TPHCM, VN'
}
// thêm 1 key mới vào object
myInfo.email = 'aphz199910@gmail.com'
console.log(myInfo)

// xóa 1 key trong object
delete myInfo.email;
console.log(myInfo)

// function trong object
myInfo.getName = function(){
    return this.name
}
console.log('ten toi la' + ' ' + myInfo.getName())

// Function in object --> Phương thức / Method
// Other in object --> Thuộc tính / Property